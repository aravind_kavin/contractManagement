function toggle_timer(timerListDataResp, client){
  var create = true;
  client.data.get("ticket")
  .then(function(ticketData){
    timerListDataResp.map(function(timerData){
      if(timerData.ticket_id == ticketData.ticket.id && create == true)
      {
        create = false;
        if(timerData.timer_running){
          $("#toggle").show();
          $("#toggle").val(timerData.id);
          $('#toggle').addClass('btn-danger').removeClass('btn-success');
          $("#toggle").text("Stop Timer");
        }
        else
        {
          $("#toggle").show();
          $("#toggle").val(timerData.id);
          $('#toggle').addClass('btn-success').removeClass('btn-danger');
          $("#toggle").text("Start Timer");
        }
      }
    });
    if(create){
      $("#create").show();
    }
  },
  function(ticketErr){
    $('#apptext').text("Error in fetching ticket data - " + ticketErr.message);
  });
}


function company_timer(iparams, data, client){
  var timerUrl = "https://"+iparams.subdomain+".freshdesk.com/api/v2/time_entries?company_id="+data.company.id;
  var timerOptions = {
    headers: {
      'Content-Type' : 'application/json',
      'Authorization': '<%=  encode(iparam.apikey) %>'
    }
  };
  client.request.get(timerUrl,timerOptions)
  .then(function(timerListData){
    var timerListDataResp = JSON.parse(timerListData.response);
      if(timerListDataResp.length > 0){

        client.data.get('ticket').then(
          function(ticketData){
          client.db.get(ticketData.ticket.id).then (
            function() {
              toggle_timer(timerListDataResp, client);
            },
            function(error){
              if(error.status == 404){
                $("#create").show();
              } 
            });
          },
          function(error){
            $('#apptext').text("Error in fetching ticket data - " + error.message);
          });
      }else{
        $("#create").show();
      }
    },
  function(timerListDataErr) {
      $('#apptext').text("Error in fetching timer list datas - " + timerListDataErr.response);
  });
}

function alert_agent(client,data,iparams){
  var companyUrl = "https://"+iparams.subdomain+".freshdesk.com/api/v2/companies/"+data.company.id;
  var companyOptions = {
    headers: {
      'Content-Type' : 'application/json',
      'Authorization': '<%=  encode(iparam.apikey) %>'
    }
  };
  client.request.get(companyUrl,companyOptions)
  .then(function(companyData){
    var parsedCompanyData = JSON.parse(companyData.response);
    var split_hr_min = parsedCompanyData.custom_fields.contract_time.split(".");
    var in_mins = Math.round(60*(split_hr_min[1]/100));

    if(parsedCompanyData.custom_fields.contract_time < iparams.alertAgentHours){
      $("#alerttext").text("Remaining contract hours : " + split_hr_min[0] + " hours and " + (in_mins ? in_mins : "00") +" minutes");
    }
    else
    {
      $("#alerttextgreen").text("Remaining contract hours : " + split_hr_min[0] + " hours and " + (in_mins ? in_mins : "00") +" minutes");
    }
  },
  function(error){
    $('#apptext').text("Error in fetching company data - " + error.response);
  });
}


function check_agent_timer(data,client){
  if(!$.isEmptyObject(data.company))
  {
    client.iparams.get().then (
      function(iparams) {
        var company_string = data.company.id.toString();
        if(iparams.companies.includes(company_string))
        {
          client.data.get("loggedInUser").then(
            function(agent){
              var timerAgentUrl = "https://"+iparams.subdomain+".freshdesk.com/api/v2/time_entries?agent_id="+agent.loggedInUser.id;
              var timerAgentOptions = {
                headers: {
                  'Content-Type' : 'application/json',
                  'Authorization': '<%=  encode(iparam.apikey) %>'
                }
              };
              client.request.get(timerAgentUrl,timerAgentOptions)
              .then(function(timerAgentData){
                  var parsedTimerAgentData = JSON.parse(timerAgentData.response);
                      if(parsedTimerAgentData[0] && parsedTimerAgentData[0].timer_running){
                        client.data.get('ticket').then(
                          function(ticketData){
                            if(parsedTimerAgentData[0].ticket_id == ticketData.ticket.id)
                            {
                              if(iparams.alertAgents){
                                alert_agent(client,data,iparams);
                              }
                              company_timer(iparams, data, client);
                            }
                            else
                            {
                              $('#apptext').text("Please stop the timer already running in ticket " + parsedTimerAgentData[0].ticket_id + " and refresh the page to start the timer for this ticket.");
                            }
                          },
                          function(error){
                            $('#apptext').text("Error in fetching ticket data - " + error.message);
                          });
                      }
                      else
                      {
                        if(iparams.alertAgents){
                          alert_agent(client,data,iparams);
                        }
                        company_timer(iparams, data, client);
                      }
                },
              function(timerAgentDataErr) {
                  $('#apptext').text("Error in fetching timer list data - " + timerAgentDataErr.response);
              });
            },
            function(error){
              $('#apptext').text("Error in fetching agent data - " + error.message);
            });

        }else{
          $('#apptext').text("Company not present under contract");
        }
      },
      function(error) {
        $('#apptext').text("Error in fetching iparams - " + error.message);
      }
    );
  }
  else
  {
    $('#apptext').text("Company not linked to the contact");
  }
}




$(document).ready( function() {
  app.initialized()
  .then(function(_client) {
    var client = _client;
    $("#create").hide();
    $("#toggle").hide();
    client.events.on('app.activated',
      function() {
      client.data.get("company").then (
        function(data) {
          check_agent_timer(data,client);
        },
        function(error) {
          $('#apptext').text("Error in fetching company data - " + error.message);
      });
      $("#create").click(function(){
        client.data.get("ticket").then (
          function(ticketdata) {
            client.iparams.get().then (
              function(iparams) {
                var createtimerUrl = "https://"+iparams.subdomain+".freshdesk.com/api/v2/tickets/"+ ticketdata.ticket.id+"/time_entries";
                var date = new Date();
                date.toISOString();
                var createtimerOptions = {
                  headers: {
                    'Content-Type' : 'application/json',
                    'Authorization': '<%= encode(iparam.apikey) %>'
                  },
                  body: JSON.stringify({
                    start_time : date,
                    timer_running : true
                  })
                };
              client.request.post(createtimerUrl, createtimerOptions)
              .then(function(timerListData){
                $("#toggle").show();
                $("#toggle").val(JSON.parse(timerListData.response).id);
                $('#toggle').addClass('btn-danger').removeClass('btn-success');
                $("#toggle").text("Stop Timer");
                $("#create").hide();
                client.db.set( ticketdata.ticket.id, {"time_spent": "0.00"});
                },
              function(createTimerErr)
              {
                $('#apptext').text("Error in creating timer - " + createTimerErr.response);
              });
            },
            function(iparamserror) {
              $('#apptext').text("Error in fetching iparams - " + iparamserror.message);
            });
          },
          function(ticketerror) {
            $('#apptext').text("Error in fetching ticket data - " + ticketerror.message);
          }
        );
      });
      $("#toggle").click(function(){
        client.iparams.get()
        .then(function(iparams){
          var stopUrl = "https://"+iparams.subdomain+".freshdesk.com/api/v2/time_entries/"+ $("#toggle").val() +"/toggle_timer";
          var stopOptions = {
            headers: {
              'Content-Type' : 'application/json',
              'Authorization': '<%=  encode(iparam.apikey) %>'
            }
          };
          client.request.put(stopUrl, stopOptions)
          .then(function(timerToggleData){
            var ParsedTimerToggleData = JSON.parse(timerToggleData.response);
            if( ParsedTimerToggleData.timer_running){
              $('#toggle').addClass('btn-danger').removeClass('btn-success');
              $("#toggle").text("Stop Timer");
            }
            else{
              $('#toggle').addClass('btn-success').removeClass('btn-danger');
              $("#toggle").text("Start Timer");
                var split_time = ParsedTimerToggleData.time_spent.split(":");
                var convert_mins = parseInt(split_time[1])/60;
                var convert_time = parseInt(split_time[0]) + convert_mins;
                client.db.get(ParsedTimerToggleData.ticket_id).then (
                  function(data) {
                    var new_time_array = (parseFloat(data.time_spent) + parseFloat(convert_time)).toFixed(2);
                    client.db.set( ParsedTimerToggleData.ticket_id, {"time_spent": new_time_array});
                  },
                  function(error) {
                    $('#apptext').text("Internal app error! Please reload the app - " + error.message);
                  });
            }
          },
          function(timerToggleErr){
            $('#apptext').text("Error in timer toggle API - " + timerToggleErr.response);
          });
        },
        function(iparamsErr){
          $('#apptext').text("Error in fetching iparams - " + iparamsErr.message);
        });
      });

      function minus_remaining_hours (ticketData,iparams,companyapiData, companyData){
        client.db.get(ticketData.ticket.id).then (
          function(data) {
            var remaining_hours = parseFloat(JSON.parse(companyapiData.response).custom_fields.contract_time).toFixed(2) - parseFloat(data.time_spent).toFixed(2);
            var fixed_two_time = remaining_hours.toFixed(2);
            var companyupdateURL = "https://"+iparams.subdomain+".freshdesk.com/api/v2/companies/"+companyData.company.id ;
            var companyUpdateOptions = {
                headers: {
                  'Content-Type' : 'application/json',
                  'Authorization': '<%=  encode(iparam.apikey) %>'
                },
                body: JSON.stringify({custom_fields :{contract_time : fixed_two_time.toString() }})
              };
              client.request.put(companyupdateURL, companyUpdateOptions)
            .then(function(){
              client.db.delete(ticketData.ticket.id);
              client.interface.trigger("showNotify", {type: "success", message: "Updated contract hours for " + companyData.company.name});
              $("#toggle").hide();
              $("#create").show();
              },function(updatecompanyErr){
                $('#apptext').text("Error in updating company data - " + updatecompanyErr.response);
              });
          },function(error){
            $('#apptext').text("Internal app error! Please reload the app - " + error.message);
          });
      }

      var propertyChangeCallback = function (event)
      {
        var event_data = event.helper.getData();
        if(event_data.new == 5)
        {
          client.data.get("company")
          .then(function(companyData){
            client.iparams.get()
            .then(function(iparams){
              client.data.get("ticket")
              .then(function(ticketData){
                var timerOptions = {
                    headers: {
                      'Content-Type' : 'application/json',
                      'Authorization': '<%=  encode(iparam.apikey) %>'
                    }
                  };
                var companyURL = "https://"+iparams.subdomain+".freshdesk.com/api/v2/companies/"+companyData.company.id ;
                client.request.get(companyURL, timerOptions )
                .then(function(companyapiData){
                   minus_remaining_hours (ticketData,iparams,companyapiData, companyData);
                },function(companyapiErr){
                  $('#apptext').text("Error in fetching company data - " + companyapiErr.response);
                });
              },function(ticketDataErr){
                $('#apptext').text("Error in fetching ticket data - " + ticketDataErr.message);
              });

            },function(iparamserror){
              $('#apptext').text("Error in fetching iparams - " + iparamserror.message);
            });
          }, function(companyErr){
            $('#apptext').text("Error in fetching company data - " + companyErr.message);
          });
        }
      };
      client.events.on("ticket.statusChanged", propertyChangeCallback);
    });
  });
});
